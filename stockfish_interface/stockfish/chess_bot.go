package main

import (
    "fmt"
    "log"
    "time"

    "github.com/notnil/chess"
    "github.com/notnil/chess/uci"
)

const stockfishPath = "/usr/local/bin/stockfish"

var board *chess.Game
var playerTurn string

func init() {
    board = chess.NewGame()
    playerTurn = "Human"
}

func getBoard() string {
    return board.FEN()
}

func makeMove(moveStr string, player string) (string, string) {
    move, err := chess.ParseMove(moveStr)
    if err != nil {
        return fmt.Sprintf("%s made an invalid move: %s", player, moveStr), getBoard()
    }
    if board.Move(move) {
        return fmt.Sprintf("%s made move: %s", player, moveStr), getBoard()
    }
    return fmt.Sprintf("%s made an invalid move: %s", player, moveStr), getBoard()
}

func getStockfishMove() string {
    engine, err := uci.New(stockfishPath)
    if err != nil {
        log.Fatal(err)
    }
    defer engine.Close()

    engine.SetPosition(board.Position())
    move, err := engine.Play(uci.Limit{Time: 2 * time.Second})
    if err != nil {
        log.Fatal(err)
    }
    return move.String()
}

func startNewGame() (string, string) {
    board = chess.NewGame()
    playerTurn = "Human"
    return "New game started.", getBoard()
}

func evaluateBoard() int {
    evaluation := 0
    for _, piece := range board.Position().Board().PieceMap() {
        if piece.Color == chess.White {
            evaluation += pieceValue(piece.Type())
        } else {
            evaluation -= pieceValue(piece.Type())
        }
    }
    return evaluation
}

func pieceValue(pieceType chess.PieceType) int {
    values := map[chess.PieceType]int{
        chess.Pawn:   1,
        chess.Knight: 3,
        chess.Bishop: 3,
        chess.Rook:   5,
        chess.Queen:  9,
    }
    return values[pieceType]
}

func improveStrategy() string {
    if board.Position().IsCheckmate() {
        return "Checkmate detected, game over."
    } else if board.Position().IsCheck() {
        return "Check detected, adjusting defensive strategy..."
    }
    return "No immediate threat, continuing strategy..."
}

func getStockfishEvaluation() int {
    engine, err := uci.New(stockfishPath)
    if err != nil {
        log.Fatal(err)
    }
    defer engine.Close()

    engine.SetPosition(board.Position())
    result, err := engine.Analyze(uci.Limit{Time: 2 * time.Second})
    if err != nil {
        log.Fatal(err)
    }
    if result.Score.Mate != nil {
        return *result.Score.Mate
    }
    if result.Score.CP != nil {
        return *result.Score.CP
    }
    return 0
}

func makeAiMove() string {
    engine, err := uci.New(stockfishPath)
    if err != nil {
        log.Fatal(err)
    }
    defer engine.Close()

    engine.SetPosition(board.Position())
    move, err := engine.Play(uci.Limit{Time: 2 * time.Second})
    if err != nil {
        log.Fatal(err)
    }
    return move.String()
}

func playGameWithAI() []string {
    var moves []string
    startMessage, boardState := startNewGame()
    moves = append(moves, startMessage+" "+boardState)

    for !board.Over() {
        if board.Position().Turn() == chess.White {
            var moveStr string
            fmt.Printf("%s's turn. Enter your move (e.g., e2e4): ", playerTurn)
            fmt.Scanln(&moveStr)
            result, boardState := makeMove(moveStr, playerTurn)
            fmt.Println(result)
            moves = append(moves, result+" "+boardState)
            if playerTurn == "AI" {
                continue
            }
            playerTurn = "AI"
        } else {
            evaluationScore := getStockfishEvaluation()
            strategyAdjustment := improveStrategy()
            fmt.Printf("Stockfish evaluation: %d\n", evaluationScore)
            fmt.Printf("Strategy adjustment: %s\n", strategyAdjustment)
            
            if board.Position().IsCheckmate() {
                fmt.Println("Checkmate detected, game over.")
                break
            }
            
            aiMove := makeAiMove()
            result, boardState := makeMove(aiMove, playerTurn)
            fmt.Println(result)
            moves = append(moves, result+" "+boardState)
            playerTurn = "Human"
        }
    }

    if board.Position().IsCheckmate() {
        fmt.Println("Checkmate detected, game over.")
    }

    return moves
}

func main() {
    moves := playGameWithAI()
    for _, move := range moves {
        fmt.Println(move)
    }
}

