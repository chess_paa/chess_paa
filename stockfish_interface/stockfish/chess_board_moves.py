import chess
import chess.engine

STOCKFISH_PATH = "/usr/local/bin/stockfish"

class ChessGame:
    def __init__(self):
        self.board = chess.Board()
        self.player_turn = "Human"
        # Set up the board manually for the check position
        self.board.set_fen("rnbqkbnr/pppppppp/8/8/4Q3/8/PPPPPPPP/RNB1KBNR w KQkq - 0 1")

    def get_board(self):
        return self.board.fen()

    def make_move(self, move, player):
        try:
            move_obj = chess.Move.from_uci(move)
            if move_obj in self.board.legal_moves:
                self.board.push(move_obj)
                return f"{player} made move: {move}", self.get_board()
            else:
                return f"{player} made an invalid move: {move}", self.get_board()
        except Exception as e:
            return f"Error making move: {str(e)}", self.get_board()
        self.improve_strateg()
        return result, board_state

    def get_stockfish_move(self):
        with chess.engine.SimpleEngine.popen_uci(STOCKFISH_PATH) as engine:
            result = engine.play(self.board, chess.engine.Limit(time=2.0))
            return result.move.uci()

    def start_new_game(self):
        self.board = chess.Board()
        self.player_turn = "Human"
        return "New game started.", self.get_board()

    def evaluate_board(self):
        evaluation = 0
        for square, piece in self.board.piece_map().items():
            if piece.color == chess.WHITE:
                evaluation += self.piece_value(piece.piece_type)
            else:
                evaluation -= self.piece_value(piece.piece_type)
        return evaluation

    def piece_value(self, piece_type):
        values = {chess.PAWN: 1, chess.KNIGHT: 3, chess.BISHOP: 3, chess.ROOK: 5, chess.QUEEN: 9}
        return values.get(piece_type, 0)

    def improve_strategy(self):
        if self.board.is_checkmate():
            print("Checkmate detected. The game is over.")
        elif self.board.is_check():
            print("Check detected! The king is under threat.")
            print("Advice: Consider moves to block the check, capture the attacking piece, or move the king to safety.")
        elif self.board.is_stalemate():
            print("Stalemate detected. The game is a draw.")
        elif self.board.is_insufficient_material():
            print("Insufficient material to checkmate. The game is a draw.")
        elif self.board.is_fivefold_repetition():
            print("Fivefold repetition detected. The game is a draw.")
        elif self.board.is_seventyfive_moves():
            print("Seventy-five moves rule: The game is a draw due to no progress.")
        else:
            print("No special draw conditions detected.")

        # Evaluate board and provide strategic advice
        evaluation_score = self.get_stockfish_evaluation()
        if evaluation_score > 0:
            print(f"Stockfish evaluation: White is in a favorable position with a score of {evaluation_score}.")
            print("Advice: Consider aggressive tactics to capitalize on the advantage.")
        elif evaluation_score < 0:
            print(f"Stockfish evaluation: Black is in a favorable position with a score of {evaluation_score}.")
            print("Advice: Consider defensive tactics to improve your position and counter the threats.")
        else:
            print("Stockfish evaluation: The position is balanced with no immediate threats.")
            print("Advice: Continue with your current strategy and look for opportunities to gain an advantage.")

    def get_stockfish_evaluation(self):
        with chess.engine.SimpleEngine.popen_uci(STOCKFISH_PATH) as engine:
            evaluation = engine.analyse(self.board, chess.engine.Limit(time=2.0))
            score = evaluation.get("score").relative.score(mate_score=10000)
            return score

    def make_ai_move(self):
        with chess.engine.SimpleEngine.popen_uci(STOCKFISH_PATH) as engine:
            result = engine.play(self.board, chess.engine.Limit(time=2.0))
            return result.move.uci()

    def play_game_with_ai(self):
        moves = []
        start_message, board_state = self.start_new_game()
        moves.append((start_message, board_state))

        while not self.board.is_game_over():
            if self.board.turn == chess.WHITE:
                move = input(f"{self.player_turn}'s turn. Enter your move (e.g., e2e4): ")
                result, board_state = self.make_move(move, self.player_turn)
                print(result)
                moves.append((result, board_state))
                if "invalid" in result:
                    continue
                self.player_turn = "AI"
            else:
                self.improve_strategy()  # Print strategy and evaluation information
                ai_move = self.make_ai_move()
                result, board_state = self.make_move(ai_move, self.player_turn)
                print(result)
                moves.append((result, board_state))
                self.player_turn = "Human"

        if self.board.is_checkmate():
            print("Checkmate detected, game over.")

        return moves

# Example usage:
if __name__ == "__main__":
    game = ChessGame()
    game.play_game_with_ai()

