from flask import Flask, request, jsonify
import chess
import chess.engine

STOCKFISH_PATH = "/usr/local/bin/stockfish"

class ChessGame:
    def __init__(self):
        self.board = chess.Board()
        self.player_turn = "Human"
        self.board.set_fen("rnbqkbnr/pppppppp/8/8/4Q3/8/PPPPPPPP/RNB1KBNR w KQkq - 0 1")

    def get_board(self):
        return self.board.fen()

    def make_move(self, move, player):
        try:
            move_obj = chess.Move.from_uci(move)
            if move_obj in self.board.legal_moves:
                self.board.push(move_obj)
                return {"status": "success", "move": move, "board": self.get_board()}
            else:
                return {"status": "error", "message": "Invalid move", "board": self.get_board()}
        except Exception as e:
            return {"status": "error", "message": str(e), "board": self.get_board()}

    def get_stockfish_move(self):
        with chess.engine.SimpleEngine.popen_uci(STOCKFISH_PATH) as engine:
            result = engine.play(self.board, chess.engine.Limit(time=2.0))
            return result.move.uci()

    def start_new_game(self):
        self.board = chess.Board()
        self.player_turn = "Human"
        return {"status": "success", "message": "New game started", "board": self.get_board()}

    def evaluate_board(self):
        evaluation = 0
        for square, piece in self.board.piece_map().items():
            if piece.color == chess.WHITE:
                evaluation += self.piece_value(piece.piece_type)
            else:
                evaluation -= self.piece_value(piece.piece_type)
        return evaluation

    def piece_value(self, piece_type):
        values = {chess.PAWN: 1, chess.KNIGHT: 3, chess.BISHOP: 3, chess.ROOK: 5, chess.QUEEN: 9}
        return values.get(piece_type, 0)

    def improve_strategy(self):
        # Evaluate board and provide strategic advice
        evaluation_score = self.get_stockfish_evaluation()
        advice = ""
        if evaluation_score > 0:
            advice = f"Stockfish evaluation: White is in a favorable position with a score of {evaluation_score}. Consider aggressive tactics."
        elif evaluation_score < 0:
            advice = f"Stockfish evaluation: Black is in a favorable position with a score of {evaluation_score}. Consider defensive tactics."
        else:
            advice = "Stockfish evaluation: The position is balanced with no immediate threats. Continue with your current strategy."
        return advice

    def get_stockfish_evaluation(self):
        with chess.engine.SimpleEngine.popen_uci(STOCKFISH_PATH) as engine:
            evaluation = engine.analyse(self.board, chess.engine.Limit(time=2.0))
            score = evaluation.get("score").relative.score(mate_score=10000)
            return score

    def make_ai_move(self):
        with chess.engine.SimpleEngine.popen_uci(STOCKFISH_PATH) as engine:
            result = engine.play(self.board, chess.engine.Limit(time=2.0))
            return result.move.uci()

    def play_game_with_ai(self):
        moves = []
        start_message = self.start_new_game()
        moves.append(start_message)

        while not self.board.is_game_over():
            if self.board.turn == chess.WHITE:
                move = self.get_stockfish_move()
                result = self.make_move(move, "AI")
                moves.append(result)
                if result['status'] == "error":
                    continue
                self.player_turn = "Human"
            else:
                self.improve_strategy()  # Print strategy and evaluation information
                ai_move = self.make_ai_move()
                result = self.make_move(ai_move, "AI")
                moves.append(result)
                self.player_turn = "Human"

        if self.board.is_checkmate():
            moves.append({"status": "game over", "message": "Checkmate detected, game over."})
        return moves

app = Flask(__name__)
game = ChessGame()

@app.route('/start', methods=['POST'])
def start_game():
    response = game.start_new_game()
    return jsonify(response)

@app.route('/move', methods=['POST'])
def move():
    data = request.json
    move = data.get('move')
    player = data.get('player', 'Human')
    response = game.make_move(move, player)
    return jsonify(response)

@app.route('/ai_move', methods=['GET'])
def ai_move():
    move = game.get_stockfish_move()
    return jsonify({"move": move})

@app.route('/evaluate', methods=['GET'])
def evaluate():
    evaluation = game.evaluate_board()
    strategy = game.improve_strategy()
    return jsonify({"evaluation": evaluation, "strategy": strategy})

@app.route('/play_with_ai', methods=['POST'])
def play_with_ai():
    result = game.play_game_with_ai()
    return jsonify(result)

if __name__ == '__main__':
    app.run(debug=True)

