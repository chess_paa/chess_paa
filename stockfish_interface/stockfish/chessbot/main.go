package main

import (
    "bufio"
    "fmt"
    "os/exec"
    "strings"
    "github.com/notnil/chess"
)

const stockfishPath = "/usr/local/bin/stockfish"

func main() {
    game := chess.NewGame()

    // Start Stockfish process
    cmd := exec.Command(stockfishPath)
    stdin, _ := cmd.StdinPipe()
    stdout, _ := cmd.StdoutPipe()
    cmd.Start()
    reader := bufio.NewReader(stdout)

    // Initialize Stockfish
    fmt.Fprintln(stdin, "uci")
    readLines(reader, 2) // Read UCI response

    fmt.Fprintln(stdin, "isready")
    readLines(reader, 1) // Read readyok

    for !game.Outcome().Valid() {
        // Get Stockfish move
        move := getStockfishMove(stdin, reader, game.Position().String())
        err := game.MoveStr(move)
        if err != nil {
            fmt.Println("Invalid move:", move)
            break
        }
        fmt.Println(game.Position().Board().Draw())
    }

    // Close Stockfish
    fmt.Fprintln(stdin, "quit")
    cmd.Wait()
}

func getStockfishMove(stdin *bufio.Writer, reader *bufio.Reader, fen string) string {
    fmt.Fprintf(stdin, "position fen %s\n", fen)
    fmt.Fprintln(stdin, "go movetime 500")
    lines := readLines(reader, 10)
    for _, line := range lines {
        if strings.HasPrefix(line, "bestmove") {
            return strings.Split(line, " ")[1]
        }
    }
    return ""
}

func readLines(reader *bufio.Reader, count int) []string {
    lines := make([]string, 0, count)
    for i := 0; i < count; i++ {
        line, err := reader.ReadString('\n')
        if err != nil {
            break
        }
        lines = append(lines, strings.TrimSpace(line))
    }
    return lines
}

