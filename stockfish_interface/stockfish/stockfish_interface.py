import subprocess

class Stockfish:
    def __init__(self, path="stockfish"):
        self.path = path
        self.process = subprocess.Popen(
            self.path,
            universal_newlines=True,
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
        )

    def send_command(self, command):
        self.process.stdin.write(command + '\n')
        self.process.stdin.flush()

    def get_output(self):
        return self.process.stdout.readline().strip()

    def set_position(self, moves):
        self.send_command(f"position startpos moves {' '.join(moves)}")

    def get_best_move(self):
        self.send_command("go movetime 2000")
        while True:
            text = self.get_output()
            if text.startswith("bestmove"):
                return text.split()[1]

def verify_uci(stockfish):
    stockfish.send_command("uci")
    while True:
        output = stockfish.get_output()
        print(output)
        if output == "uciok":
            break

def verify_isready(stockfish):
    stockfish.send_command("isready")
    while True:
        output = stockfish.get_output()
        print(output)
        if output == "readyok":
            break

def set_position_and_get_best_move(stockfish, moves):
    stockfish.set_position(moves)
    best_move = stockfish.get_best_move()
    print(f"Best move: {best_move}")


stockfish = Stockfish()
verify_uci(stockfish)
verify_isready(stockfish)
set_position_and_get_best_move(stockfish, ["e2e4", "e7e5", "g1f3", "b8c6"])

