from flask import Flask, request, jsonify
import chess
import chess.engine

app = Flask(__name__)

STOCKFISH_PATH = "/usr/local/bin/stockfish"
board = chess.Board()
player_turn = "Human"

def get_board():
    return board.fen()

def make_move(move, player):
    if chess.Move.from_uci(move) in board.legal_moves:
        board.push(chess.Move.from_uci(move))
        return f"{player} made move: {move}", get_board()
    else:
        return f"{player} made an invalid move: {move}", get_board()

def get_stockfish_move():
    with chess.engine.SimpleEngine.popen_uci(STOCKFISH_PATH) as engine:
        result = engine.play(board, chess.engine.Limit(time=2.0))
        return result.move.uci()

def start_new_game():
    global board, player_turn
    board = chess.Board()
    player_turn = "Human"
    return "New game started.", get_board()

def piece_value(piece_type):
    values = {chess.PAWN: 1, chess.KNIGHT: 3, chess.BISHOP: 3, chess.ROOK: 5, chess.QUEEN: 9}
    return values.get(piece_type, 0)

def evaluate_board():
    evaluation = 0
    for square, piece in board.piece_map().items():
        if piece.color == chess.WHITE:
            evaluation += piece_value(piece.piece_type)
        else:
            evaluation -= piece_value(piece.piece_type)
    return evaluation

def improve_strategy():
    if board.is_checkmate():
        return "Checkmate detected, game over."
    elif board.is_check():
        return "Check detected, adjusting defensive strategy..."
    else:
        return "No immediate threat, continuing strategy..."

def get_stockfish_evaluation():
    with chess.engine.SimpleEngine.popen_uci(STOCKFISH_PATH) as engine:
        evaluation = engine.analyse(board, chess.engine.Limit(time=2.0))
        return evaluation.get("score").relative.score(mate_score=10000)

@app.route('/new_game', methods=['POST'])
def new_game():
    message, board_state = start_new_game()
    return jsonify({"message": message, "board": board_state})

@app.route('/make_move', methods=['POST'])
def make_move_route():
    data = request.json
    move = data.get("move")
    player = data.get("player", "Human")
    result, board_state = make_move(move, player)
    return jsonify({"result": result, "board": board_state})

@app.route('/stockfish_move', methods=['POST'])
def stockfish_move():
    data = request.json
    board_state = data.get("board")

    # Set the board to the received state
    board.set_fen(board_state)

    move = get_stockfish_move()
    return jsonify({"stockfish_move": move})

@app.route('/evaluate', methods=['GET'])
def evaluate():
    evaluation = evaluate_board()
    return jsonify({"evaluation": evaluation})

@app.route('/improve_strategy', methods=['GET'])
def improve_strategy_route():
    strategy = improve_strategy()
    return jsonify({"strategy": strategy})

@app.route('/stockfish_evaluation', methods=['GET'])
def stockfish_evaluation():
    evaluation = get_stockfish_evaluation()
    return jsonify({"stockfish_evaluation": evaluation})

@app.route('/play_game', methods=['POST'])
def play_game():
    global player_turn
    moves = []
    start_message, board_state = start_new_game()
    moves.append({"message": start_message, "board": board_state})

    while not board.is_game_over():
        if board.turn == chess.WHITE:
            move = request.json.get("move")
            result, board_state = make_move(move, player_turn)
            moves.append({"result": result, "board": board_state})
            if "invalid" in result:
                continue
            player_turn = "AI"
        else:
            evaluation_score = get_stockfish_evaluation()
            strategy_adjustment = improve_strategy()
            ai_move = get_stockfish_move()
            result, board_state = make_move(ai_move, player_turn)
            moves.append({"result": result, "board": board_state})
            player_turn = "Human"

    return jsonify(moves)

if __name__ == '__main__':
    app.run(debug=True)

