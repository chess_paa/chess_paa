# chess_paa- Meet the team
This is a project by team "T3 Coders" titled Chess Paa. It's basically a grandfather(AI) teaching it's kid how to play and improve in chess.

Team Members:
Prashansa Bhatia - working on AI/ML 
Kajal Jotwani - working on the frontend with HTML, CSS , JS and React
Riddhi Poddar - working on backend with GO

# Overview

This project is an interactive chess game featuring an AI-powered bot, real-time move validation, and FIDE rating prediction based on game performance. The frontend is built using React, and the backend is developed with Python.
Features
Interactive Chess Gameplay

    Play chess against an AI-powered bot.
    Real-time move validation and responses using the Stockfish engine.

# Rating Prediction

    Predict users’ FIDE ratings based on game performance.
    Machine learning model to analyze game data and provide accurate predictions.

# Aim

We are embarking on an exciting project to expand our skills by diving into entirely new frameworks:

    Frontend: React
    Backend: Python and Python packages
    AI/ML Component: Sophisticated chess bot development

# Tool Stack

    Programming Language: Python
    Database: PostgreSQL
    Frontend: React
    Chessboard: chessboard.js
    Chess Engine: StockFish
    Machine Learning: Scikit-Learn, TensorFlow

# Mockup/Proof of Concept (PoC)
Mockup

    Create wireframes or detailed UI mockups showing the chessboard interface, game controls, and user profile screens in React.

Proof of Concept

    Implement a basic Python backend with endpoints for user authentication, starting a new game, making moves, and retrieving game history.
    Integrate a simple version of Stockfish for move validation and basic AI gameplay.

# Installation
Backend (Python)

    Clone the repository:

    bash

git clone https://github.com/your-repo/chess-engine.git
cd chess-engine/backend

Create a virtual environment and activate it:

bash

python -m venv venv
source venv/bin/activate

Install dependencies:

bash

pip install -r requirements.txt

Start the backend server:

bash

    python app.py

Frontend (React)

    Navigate to the frontend directory:

    bash

cd ../frontend

Install dependencies:

bash

npm install

Start the development server:

bash

    npm start

# React + Vite
This template provides a minimal setup to get React working in Vite with HMR and some ESLint rules.
Currently, two official plugins are available:


@vitejs/plugin-react uses Babel for Fast Refresh
@vitejs/plugin-react-swc uses SWC for Fast Refresh

# Usage
Backend (Python)

The backend is a simple Bottle server that provides several endpoints:

    GET /: This endpoint returns a welcome message. It's a basic health check to ensure the server is running.

    POST /gen_move: This endpoint is used to generate a move based on the current state of the chessboard, which is sent in the request body as a FEN string. The backend uses the Stockfish engine to compute the best move and returns the updated board state.

Frontend (React)

The frontend is a React application that provides the user interface for playing chess. It includes:

    index.html: The main HTML file that sets up the root div where the React app will be rendered.

    index.css: The main CSS file that includes styles for the application, ensuring a consistent and visually appealing design.

    main.jsx: The entry point for the React application. It renders the main App component into the root div defined in index.html. It also includes performance monitoring setup with reportWebVitals.

# Acknowledgments

    Stockfish
    chessboard.js
    React
    Scikit-Learn
    TensorFlow
